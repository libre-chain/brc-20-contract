#include <eosio.token/eosio.token.hpp>

#include <bech32.h>
#include <base58.h>

namespace eosio
{

   void token::create(const name &issuer,
                      const asset &maximum_supply)
   {
      require_auth(get_self());

      auto sym = maximum_supply.symbol;
      check(sym.is_valid(), "invalid symbol name");
      check(maximum_supply.is_valid(), "invalid supply");
      check(maximum_supply.amount > 0, "max-supply must be positive");

      stats statstable(get_self(), sym.code().raw());
      auto existing = statstable.find(sym.code().raw());
      check(existing == statstable.end(), "token with symbol already exists");

      statstable.emplace(get_self(), [&](auto &s)
                         {
       s.supply.symbol = maximum_supply.symbol;
       s.max_supply    = maximum_supply;
       s.issuer        = issuer; });
   }

   void token::issue(const name &to, const asset &quantity, const string &memo)
   {
      auto sym = quantity.symbol;
      check(sym.is_valid(), "invalid symbol name");
      check(memo.size() <= 256, "memo has more than 256 bytes");
      check(is_account(to), "to account does not exist");

      stats statstable(get_self(), sym.code().raw());
      auto existing = statstable.find(sym.code().raw());
      check(existing != statstable.end(), "token with symbol does not exist, create token before issue");
      const auto &st = *existing;

      require_auth(st.issuer);
      check(quantity.is_valid(), "invalid quantity");
      check(quantity.amount > 0, "must issue positive quantity");

      check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");
      check(quantity.amount <= st.max_supply.amount - st.supply.amount, "quantity exceeds available supply");

      statstable.modify(st, same_payer, [&](auto &s)
                        { s.supply += quantity; });

      add_balance(to, quantity, st.issuer);
   }

   void token::retire(const asset &quantity, const string &memo)
   {
      auto sym = quantity.symbol;
      check(sym.is_valid(), "invalid symbol name");
      check(memo.size() <= 256, "memo has more than 256 bytes");

      stats statstable(get_self(), sym.code().raw());
      auto existing = statstable.find(sym.code().raw());
      check(existing != statstable.end(), "token with symbol does not exist");
      const auto &st = *existing;

      require_auth(st.issuer);
      check(quantity.is_valid(), "invalid quantity");
      check(quantity.amount > 0, "must retire positive quantity");

      check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");

      statstable.modify(st, same_payer, [&](auto &s)
                        { s.supply -= quantity; });

      sub_balance(st.issuer, quantity);
   }

   void token::redeem(const name sender, const asset &quantity, const string &btc_address)
   {
      require_auth(sender);

      auto sym = quantity.symbol;
      check(sym.is_valid(), "invalid symbol name");

      check(is_valid_bitcoin_address(btc_address), "btc_address is not a valid bitcoin address");

      stats statstable(get_self(), sym.code().raw());
      auto existing = statstable.find(sym.code().raw());
      check(existing != statstable.end(), "token with symbol does not exist");
      const auto &st = *existing;

      check(quantity.is_valid(), "invalid quantity");
      check(quantity.amount > 0, "must redeem positive quantity");

      check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");

      statstable.modify(st, same_payer, [&](auto &s)
                        { s.supply -= quantity; });

      sub_balance(sender, quantity);
   }

   void token::transfer(const name &from,
                        const name &to,
                        const asset &quantity,
                        const string &memo)
   {
      check(from != to, "cannot transfer to self");
      require_auth(from);
      check(is_account(to), "to account does not exist");
      auto sym = quantity.symbol.code();
      stats statstable(get_self(), sym.raw());
      const auto &st = statstable.get(sym.raw());

      require_recipient(from);
      require_recipient(to);

      check(quantity.is_valid(), "invalid quantity");
      check(quantity.amount > 0, "must transfer positive quantity");
      check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");
      check(memo.size() <= 256, "memo has more than 256 bytes");

      auto payer = has_auth(to) ? to : from;

      sub_balance(from, quantity);
      add_balance(to, quantity, payer);
   }

   void token::sub_balance(const name &owner, const asset &value)
   {
      accounts from_acnts(get_self(), owner.value);

      const auto &from = from_acnts.get(value.symbol.code().raw(), "no balance object found");
      check(from.balance.amount >= value.amount, "overdrawn balance");

      from_acnts.modify(from, owner, [&](auto &a)
                        { a.balance -= value; });
   }

   void token::add_balance(const name &owner, const asset &value, const name &ram_payer)
   {
      accounts to_acnts(get_self(), owner.value);
      auto to = to_acnts.find(value.symbol.code().raw());
      if (to == to_acnts.end())
      {
         to_acnts.emplace(ram_payer, [&](auto &a)
                          { a.balance = value; });
      }
      else
      {
         to_acnts.modify(to, same_payer, [&](auto &a)
                         { a.balance += value; });
      }
   }

   void token::open(const name &owner, const symbol &symbol, const name &ram_payer)
   {
      require_auth(ram_payer);

      check(is_account(owner), "owner account does not exist");

      auto sym_code_raw = symbol.code().raw();
      stats statstable(get_self(), sym_code_raw);
      const auto &st = statstable.get(sym_code_raw, "symbol does not exist");
      check(st.supply.symbol == symbol, "symbol precision mismatch");

      accounts acnts(get_self(), owner.value);
      auto it = acnts.find(sym_code_raw);
      if (it == acnts.end())
      {
         acnts.emplace(ram_payer, [&](auto &a)
                       { a.balance = asset{0, symbol}; });
      }
   }

   void token::close(const name &owner, const symbol &symbol)
   {
      require_auth(owner);
      accounts acnts(get_self(), owner.value);
      auto it = acnts.find(symbol.code().raw());
      check(it != acnts.end(), "Balance row already deleted or never existed. Action won't have any effect.");
      check(it->balance.amount == 0, "Cannot close because the balance is not zero.");
      acnts.erase(it);
   }

   void token::add(const name &name, const string &bitcoin_name, const asset &max_supply, uint64_t inscription_number)
   {
      require_auth(get_self());

      auto sym = max_supply.symbol;

      check(bitcoin_name.size() > 0 && bitcoin_name.size() < 8, "btc name cannot be empty and max lenght is 8 characters");
      check(sym.is_valid(), "invalid symbol name");
      check(max_supply.is_valid(), "invalid supply");
      check(max_supply.amount > 0, "max-supply must be positive");

      references reference_tb(get_self(), name.value);
      auto reference_itr = reference_tb.find(name.value);

      check(reference_itr == reference_tb.end(), "reference already exists");

      reference_tb.emplace(get_self(), [&](auto &row)
                           {
          row.name = name;
          row.bitcoin_name = bitcoin_name;
          row.max_supply = max_supply;
          row.inscription_number = inscription_number; });
   }

   void token::modify(const name &name, const string &bitcoin_name, const asset &max_supply, uint64_t inscription_number)
   {
      require_auth(get_self());

      auto sym = max_supply.symbol;

      check(bitcoin_name.size() > 0 && bitcoin_name.size() < 8, "btc name cannot be empty and max lenght is 8 characters");
      check(sym.is_valid(), "invalid symbol name");
      check(max_supply.is_valid(), "invalid supply");
      check(max_supply.amount > 0, "max-supply must be positive");

      references reference_tb(get_self(), name.value);
      auto reference_itr = reference_tb.find(name.value);

      check(reference_itr != reference_tb.end(), "reference does not exists");

      reference_tb.modify(reference_itr, get_self(), [&](auto &row)
                           {
          row.bitcoin_name = bitcoin_name;
          row.max_supply = max_supply;
          row.inscription_number = inscription_number; });
   }

   void token::remove(const name &name)
   {
      require_auth(get_self());

      references reference_tb(get_self(), name.value);
      auto reference_itr = reference_tb.find(name.value);

      check(reference_itr != reference_tb.end(), "reference does not exists");

      reference_tb.erase(reference_itr);
   }

   // Function to validate a Bitcoin address
   bool token::is_valid_bitcoin_address(const string &btc_address)
   {
      // Check if address has the correct length
      int len = btc_address.length();
      if (len != 34 && len != 42)
      {
         return false;
      }

      // Check if btc_address starts with the correct prefix
      if (btc_address[0] == '1' || btc_address[0] == '3')
      {
         // Legacy (P2PKH) or Pay to Script Hash (P2SH)
         std::vector<unsigned char> vchRet;
         return DecodeBase58(btc_address, vchRet, 64);
      }
      else if (btc_address.substr(0, 3) == "bc1")
      {
         bech32::DecodeResult res = bech32::Decode(btc_address);
         if (res.encoding == bech32::Encoding::INVALID)
         {
            return false;
         }
      }

      return true;
   }

} /// namespace eosio
